import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  messages: string[] = []

  // Adds message to the cache
  add(message: string) {
    this.messages.push(message)
  }

  // Clears message to the cache
  clear() {
    this.messages = []
  }
}
